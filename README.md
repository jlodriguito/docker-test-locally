This is spin off Test Local experiment ( SIMPLE PYTHON WEB DAEMON )

Involves Fuji docker image
Simple Start up script using Procfile ( single instancy type )

Steps:

On your PC/Mac install docker machine, docker, and Virtual Box

Using Brew ( MAC )

brew install docker
brew install docker-machine
brew tap caskroom/cask
brew cask install virtualbox
docker-machine create --driver virtualbox default

Additional Steps

eval "$(docker-machine env default)"
docker-machine ip default

FUJI IMAGE

Download Fuji docker image from Production repository ( Needs Access )

PULL TEST FILES

git clone <this git repo>

HOW TO TEST:


docker run -it -v $(pwd):/app -e 'PORT=5000' -p 80:5000 <DOCKER SOURCE>/pie/fuji bash start exec

Port 5000 is bound to port 80 ( accessible from MAC/PC Browser )
